from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('<int:id>/', views.grocery_item_details, name = 'grocery_item_details'),
]
