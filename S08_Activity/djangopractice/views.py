from django.shortcuts import render
from .models import GroceryItem

# Create your views here.
def index(request):
    template = 'djangopractice/index.html'
    items = GroceryItem.objects.all()
    context = {'items': items}
    return render(request, template, context)

def grocery_item_details(request, id):
    template = 'djangopractice/grocery_item_details.html'

    try:
        item = GroceryItem.objects.get(id = id)
    except GroceryItem.DoesNotExist:
        item = None
    
    context = {'item': item, 'id': id}  
    return render(request, template, context)