from django.shortcuts import render
from django.http import HttpResponse
from .models import ToDoItem

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    context = { 'todoitem_list': todoitem_list }
    return render(request, 'todolist/index.html', context)


def todoitem(request, todoitem_id):
    response = f"You are viewing the details of {todoitem_id}"
    return HttpResponse(response)